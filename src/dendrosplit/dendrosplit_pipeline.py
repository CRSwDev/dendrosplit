import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import split
import merge
import preprocessing
from utils import *
import numpy as np
import scipy.sparse
import os
import os.path
import glob
import argparse
import csv
import sys
import time


def main():
    des = 'Clustering Analysis using Recursive Dendrogram Splitting and Merging Algorithm, Version 1.0. This algorithm has been used in the BD Rhapsody Clustering Analysis pipeline.'
    parser = argparse.ArgumentParser(description=des, add_help=True)

    parser.add_argument('--datatable', action='store', dest='data_table', required=True,
                        help='Name(s) of the datatable file either in dense matrix format usually with suffix .csv (such as MolsPerCell.csv file generated from the BD Rhapsody pipeline) '
                             'or in sparse matrix format usually with suffix .st (such as Expression_Matrix.st file generated from Rhapsody pipeline). '
                             'Can accept multiple datatable files, but all need to be of the same type (either all in '
                             'dense or sparse matrix format); mulitple datatable files should be comma separated.')
    parser.add_argument('--startcol-dm', action='store', type=int, dest='startcol_dm', default=1,
                        help='Specify from which column the actual data are stored, 0-based indexing. '
                             'For example, if the first column is the cell identity information, and the actual data are stored '
                             'starting from the second column, specify startcol-dm as 1.')
    parser.add_argument('--col-sm', action='store', type=int, dest='col_sm', default=6,
                        help='Specify the column that stores the actual data used for clustering analysis, 0-based indexing. '
                             'For example, if the 7th column stores the data for analysis, specify col-sm as 6.')

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    data_table = args.data_table
    table_list = data_table.split(',')
    output_header = skip_main_header(table_list[0])
    startcol_dm = args.startcol_dm
    col_sm = args.col_sm

    start = time.strftime("%Y-%m-%d %H:%M:%S")
    print "Start clustering analysis: {}".format(start)

    # create a dir to store the results from clustering analysis
    directory = 'ClusteringAnalysis'
    if not os.path.exists(directory):
        os.makedirs(directory)

    # check to make sure only one type of datatable files specified
    extension = []
    for table in table_list:
        extension.append(table.lower().split('.')[1])
        if len(set(extension)) > 1:
            sys.exit('Multiple data table types specified. Make sure input consists of only dense matrix or only '
                     'sparse matrix formatted files.')

    # combine data tables if multiple files provided
    if len(table_list) != 1:
        DT, sample, output_header = combine_samples(table_list, extension[0].lower(), directory)
    else:
        DT = table_list[0]
        sample = DT.split('_')[0]

    # load datatable file
    if extension[0].lower() == 'csv':
        X, genes = load_dm(DT, len(output_header), startcol_dm)
    elif extension[0].lower() == 'st':
        X, genes = load_sm(DT, len(output_header), col_sm)

    # apply gene filter 
    X, genes = split.filter_genes(X, genes)

    # calculate tSNE coordinates, will be used later for visualization of clustering results
    tsne_file = os.path.join(directory, sample + '_bh-tSNEcoordinates.csv')
    # program errors out if nrow(X) - 1 < 3 * perplexity, add checkpoint so that it exits successfully
    perplexity = 15.0
    if X.shape[0] < perplexity * 3 + 1:
        print "Too few data points. Perplexity is too large for the number of data points!"
        sys.exit()
    x1, x2 = preprocessing.low_dimensional_embedding2(np.log(X + 1), pcs=50, per=perplexity)
    coord_header = '\n'.join([item for sublist in output_header for item in sublist]) + '\n' + 'Coordinate_1,Coordinate_2'
    np.savetxt(tsne_file, np.c_[x1, x2], delimiter=',', fmt='%.5f', header=coord_header, comments='')

    # perform the clustering analysis
    run_dendrosplit(X, genes, x1, x2, directory, sample, output_header)

    # compress clustering results into one zip file
    cmd = 'zip -r {0}.zip {0}'.format(directory)
    os.system(cmd)
    print "Finished Clustering Analysis: {}".format(time.strftime("%Y-%m-%d %H:%M:%S"))
    return 0


def run_dendrosplit(X, genes, x1, x2, dir, sample, output_header):
    D = split.log_correlation(X)
    ys, shistory = split.dendrosplit((D, X), preprocessing='precomputed', score_threshold=5, verbose=False,
                                     disband_percentile=50)
    ys_sweep = []

    thresholds = list(range(10, 120, 5))
    for threshold in thresholds:
        ys_sweep.append(split.get_clusters_from_history(D, shistory, threshold, 50))

    a = plt.figure()
    plt.plot(thresholds, [count_nonsingleton_clusters(i) for i in ys_sweep])
    plt.grid()
    plt.xlabel('Thresholds (-log10(p-value))')
    plt.ylabel('Number of non-singleton clusters')
    plt.savefig(os.path.join(dir, sample + '_numCluster_pvalueThreshold.png'), format='png', dpi=300)
    plt.close(a)

    lowest_cutoff = None
    one_cluster_only = True
    num_cluster = [count_nonsingleton_clusters(i) for i in ys_sweep]
    for i in range(0, len(thresholds)-1):
        if (i == 0 or num_cluster[i] < num_cluster[i-1]) and num_cluster[i] == num_cluster[i+1]:
            if lowest_cutoff is None:
                lowest_cutoff = thresholds[i]

            if num_cluster[i] == 1 and not one_cluster_only:
                break

            ym, mhistory = merge.dendromerge((D, X), ys_sweep[i], score_threshold=thresholds[i]/2,
                                             preprocessing='precomputed', verbose=True, outlier_threshold_percentile=50)
            ym = split.map_singleton_to_label(ym)
            num_cluster_m = len(np.unique(ym))
            if -1 in ym:
                num_cluster_m -= 1
            if num_cluster_m <= 1 and not one_cluster_only:
                break

            directory = os.path.join(dir, str(num_cluster_m) + '-Clusters')
            sample_with_clust = sample + '_' + str(num_cluster_m) + '-Clusters'

            if not os.path.exists(directory):
                os.makedirs(directory)

            one_cluster_only = False
            ys = map_label_to_nonzero(split.map_singleton_to_label(split.str_labels_to_ints(ys_sweep[i])))
            plt.figure()
            split.plot_labels_legend(x1, x2, map_label_to_nonzero(ym),
                                     title=sample + ' -- t-SNE dimension reduction with ' + str(num_cluster_m) +' clusters',
                                     save_name=os.path.join(directory, sample_with_clust + '_tSNE'))
            save_label_to_csv(os.path.join(directory, sample_with_clust), map_label_to_nonzero(ym), output_header)
            plt.close('all')

            if num_cluster_m > 1:
                plt.figure()
                split.plot_labels_legend(x1, x2, ys,
                                         title='Splitting result using a threshold of %.0f'%(thresholds[i]),
                                         save_name=os.path.join(directory, sample +'_split_tsne'))

                split.plot_dendro(D, labels=ym, save_name=os.path.join(directory, sample + '_dendrogram'))

                split.visualize_history(np.log(1+X), x1, x2, genes, mhistory,
                                        save_name=os.path.join(directory, sample + '_mergeHistory'))

                split.save_more_highly_expressed_genes_in_one_clust(X, genes, map_label_to_nonzero(ym), x1, x2,
                                                                    num_genes=50, output_header=output_header,
                                                                    save_name=os.path.join(directory, sample_with_clust),
                                                                    show_plots=False, verbose=False)

                split.pairwise_cluster_comparison(X, genes, map_label_to_nonzero(ym), x1, x2, num_genes=50,
                                                  output_header=output_header,
                                                  save_name=os.path.join(directory, sample_with_clust),
                                                  show_plots=False, verbose=False)
                plt.close('all')

    shistory = split.filter_out_extraneous_steps(shistory, lowest_cutoff)

    if len(shistory) == 0:
        return

    directory = os.path.join(dir, 'splitHistory')
    if not os.path.exists(directory):
        os.makedirs(directory)

    split.visualize_history(np.log(1+X), x1, x2, genes, shistory, score_threshold=lowest_cutoff,
                            save_name=os.path.join(directory, sample))
    plt.close('all')

    split.save_history(genes, shistory, score_threshold=lowest_cutoff, num_genes=50, output_header=output_header,
                       save_name=os.path.join(directory, sample))

    return


def combine_samples(table_list, extension, out_dir):
    names = []
    for table in table_list:
        output_header = skip_main_header(table)
        sample_name = table.split('_')[0]
        names.append(sample_name)
    name = '-'.join(names)

    table_rows = []

    combined_output_header = [["####################"], ["## BD Rhapsody Clustering Analysis Version 1.0"],
                             ["## Combined Data Table"], ["####################"]]

    if extension == 'csv':
        # check to make sure the columns from different datatable files that need to be combined match
        for i in range(len(table_list)):
            with open(table_list[i], 'r') as f:
                reader = csv.reader(f)
                # extract the line that gives the column names, such as cell indentity and gene names
                for line_idx in range(len(output_header) + 1):
                    header = reader.next()
                if i == 0:
                    column_names = header
                elif header != column_names:
                    print 'Column names in input files do not match!'
                    sys.exit(1)

        combined_DT = os.path.join(out_dir, name + '_Combined.csv')
        with open(combined_DT, 'w') as g:
            rb = csv.writer(g)
            for row in combined_output_header:
                rb.writerow(row)

            # output the row with column names
            rb.writerow(column_names)

            # add sample label to cell identity and append to combined file
            for table_idx, DT_mol in enumerate(table_list):
                h = open(DT_mol, 'r')
                reader = csv.reader(h)
                # skip the output_header and the row with the column names
                for i in range(len(output_header) + 1):
                    reader.next()
                for read in reader:
                    read[0] += '--' + names[table_idx]
                    rb.writerow(read)
                h.close()

    elif extension == 'st':
        for i in range(len(table_list)):
            print 'Loading data from: {}' .format(os.path.basename(table_list[i]))
            data = np.genfromtxt(table_list[i], dtype=str, delimiter='\t', skip_header=len(output_header))
            # extract the line with column names
            line_with_colnames = data[0,]
            if i == 0:
                header_columns = line_with_colnames
            elif not np.array_equal(line_with_colnames, header_columns):
                print 'Column names in input files do not match!'
                sys.exit(1)
            # the actual data start from 2nd row of loaded data
            data = data[1:,]
            genes = np.unique(data[:, 1])

            # append sample label to cell identity column
            sample_id = '--' + names[i]
            labels = []
            for elem in data[:, 0]:
                elem += sample_id
                labels.append(elem)

            array_with_labels = np.column_stack((labels, data[:,1:]))

            if i == 0:
                gene_panel = genes.tolist()
                conc_array = array_with_labels
            else:
                # append to gene panel and array
                for gene in genes:
                    if gene not in gene_panel:
                        gene_panel.append(gene)
                conc_array = np.vstack((conc_array, array_with_labels))

        combined_DT = os.path.join(out_dir, name + '_Combined.st')
        header = '\n'.join([item for sublist in combined_output_header for item in sublist]) + '\n'
        header += '\t'.join(line_with_colnames)
        np.savetxt(combined_DT, conc_array, delimiter='\t', newline='\n', fmt="%s", header=header, comments='')

    return combined_DT, name, combined_output_header


if __name__ == '__main__':
    sys.exit(main())

