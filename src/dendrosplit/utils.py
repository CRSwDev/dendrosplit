import colorsys
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import pandas as pd
import sys
import scipy.sparse


# When printing, decide whether to use scientific notation (if value gets too big)
def sn(i,j=2):
    if i > 1000 or i < 0.01: return '%.1E'%(i)
    s = '%.'+str(j)+'f'
    return s%(i)


# read the data file and return the header lines
def skip_main_header(file):
    # get main output header from file
    with open(file, 'r') as f:
        output_header = []
        header = True
        while header is True:
            line = next(f)
            if line.startswith("##"):
                output_header.append([line.split(',')[0].strip()])
            else:
                header = False
    return output_header


# load the datatable in the dense matrix format
def load_dm(flname, len_header, start_col_to_use):
    # len_header: lines of headers to be skipped
    # start_col_to_use: the first column of the data to be used for clustering analysis
    data = np.genfromtxt(flname, dtype=float, delimiter=',', names=True, deletechars='', skip_header=len_header)
    X = data.view(np.float64).reshape(data.shape + (-1,))[:,start_col_to_use:]
    genes = np.array(data.dtype.names[start_col_to_use:])
    print np.shape(X), np.shape(genes)
    return X, genes


# load the datatable in the sparse matrix format
def load_sm(flname, len_header, col_to_use):
    # col_to_use: the column index of the data to be used for clustering analysis
    data = np.genfromtxt(flname, dtype=str, delimiter='\t', skip_header=len_header+1, deletechars='')
    cl, cl_ind, ind_i = np.unique(data[:, 0], return_index=True, return_inverse=True)
    cl_order = np.argsort(cl_ind)
    cl = cl[cl_order]
    ind_i = np.array([np.where(cl_order == i)[0][0] for i in ind_i])
    genes, ind_j = np.unique(data[:, 1], return_inverse=True)
    sp_matrix = scipy.sparse.coo_matrix((data[:, col_to_use].astype(np.float64), (ind_i, ind_j)),
                                        shape=(len(cl), len(genes)))
    return sp_matrix.toarray(), genes


# load the tSNE coordinates
def load_coordinates(flname, len_header):
    X_coor = np.genfromtxt(flname, delimiter=',', skip_header=len_header + 1)
    x1, x2 = X_coor[:, 0], X_coor[:, 1]
    return x1,x2


# count number of clusters that contain more than 1 element contained in the cluster
def count_nonsingleton_clusters(y):
    return sum([1 for i in np.unique(y) if np.sum(y == i) != 1])


# save cluster labels to a csv file
def save_label_to_csv(flname, label, output_header):
    header = '\n'.join([item for sublist in output_header for item in sublist]) + '\n' + 'Cluster_Label'
    np.savetxt(flname+'_Labels.csv', label.astype(int), fmt='%d', header=header, comments='')
    return


# convert the label to a non-zero number
def map_label_to_nonzero(y):
    return np.array([l+1 if l != -1 else l for l in y])


# plot cluster labels in figure legend
def plot_labels_legend(x1,x2,y,labels=None,title=None,save_name=None,label_singletons=True):
    if label_singletons:
        y=map_singleton_to_label(y)
    Ncolors = len(np.unique(y))
    if labels is None: labels = np.unique(y)
    HSVs = [(x*1.0/Ncolors, 0.8, 0.9) for x in range(Ncolors)]
    RGBs = map(lambda x: colorsys.hsv_to_rgb(*x), HSVs)
    for j,i in enumerate(labels):
        if i != -1:
            lab = 'Cluster #{}\nCells: {}'.format(str(i), str(np.sum(y==i)))
            plt.plot(x1[y==i], x2[y==i], '.', c=RGBs[j], label=lab)
        else:
            plt.plot(x1[y==i], x2[y==i], '.', c=RGBs[j], label='Singletons:'+str(np.sum(y==i)), markeredgecolor='k')
    _ = plt.axis('off')
    plt.subplots_adjust(left=0.05, right=0.7, top=0.95, bottom=0.05)
    plt.legend(bbox_to_anchor=(1.4, 1.0), prop={'size': 10}, frameon=False, columnspacing=2, numpoints=1,
               markerscale=3, labelspacing=1)
    if title:
        plt.title(title)
    if save_name is not None:
        plt.savefig(save_name+'.png', format='png', bbox_inches='tight', dpi=300)


# For each feature, determine the index of the cluster with the greatest expression
def compare_feature_means(X,Y):
    Nc = len(np.unique(Y))
    N,M = np.shape(X)
    m = np.zeros((Nc,M))
    for i,c in enumerate(np.unique(Y)):
        m[i,:] = np.mean(X[Y == c,:],0)
    return np.argmax(m,0)


# Map labels to integers
def str_labels_to_ints(y_str):
    y_int = np.zeros(len(y_str))
    for i,label in enumerate(np.unique(y_str)):
        y_int[y_str == label] = i
    return y_int.astype(int)


# Get all off-diagonal entries in a distance matrix
def flatten_distance_matrix(D,inds=None):
    if inds is not None: D2 = cut_matrix_along_both_axes(D,inds)
    else: D2 = D
    d = D2.reshape(-1,1)
    return np.delete(d,np.where(d==0)[0])


# index a 2D matrix along both axes
def cut_matrix_along_both_axes(X,inds):
    Z = X[inds,:]
    return Z[:,inds]


# map singleton string labels to -1
def map_singleton_to_label(y):
    for i,c in enumerate(np.unique(y)):
        if np.sum(y == c) == 1:
            y[y == c] = -1
    return y


# return median of a correlation distribution
def median_cdist_corr(D, i, j, z):
    dM = flatten_distance_matrix(D, np.logical_or(z == i, z == j))
    return np.median(dM)
