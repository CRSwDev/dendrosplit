#!/usr/bin/env python

from dendrosplit import split,merge
from sklearn import datasets
from sklearn.metrics.pairwise import pairwise_distances
import numpy as np

def test_synthetic_data():
    # Generate noisy circles dataset
    noisy_circles = datasets.make_circles(n_samples=500, factor=.5, noise=.05, random_state=0)
    X,Y = noisy_circles[0],noisy_circles[1]  # X is the actual data, Y is the truth for label
    genes = np.array(range(2)).astype(str)

    # Map points to distance from center
    Xse = np.linalg.norm(X,axis=1).reshape(-1,1)

    # Get distance matrix
    D = pairwise_distances(Xse)
    D = (D+D.T)/2

    # Perform clustering analysis using split and merge steps
    ys,shistory = split.dendrosplit((D, Xse),
                                    preprocessing='precomputed',
                                    score_threshold=30,
                                    disband_percentile=50,
                                    split_evaluator = split.select_genes_using_Welchs,
                                    verbose=True)

    ym, mhistory = merge.dendromerge((D, Xse), ys, 
                                     score_threshold=10,
                                     preprocessing='precomputed', 
                                     outlier_threshold_percentile=90,
                                     verbose=True)

    # Get unique labels and the counts of each label after clustering
    unique, counts = np.unique(ym, return_counts = True)
    assert len(unique) == 2
    assert counts[0] == counts[1]
 

if __name__ == '__main__':
    test_dendrosplit()
