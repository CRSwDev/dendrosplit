# README #


### What is this repository for? ###

* This repository houses a tool/python package that can be used to perform clustering analysis using the *dendrosplit* (Recursive Dendrogram Splitting) algorithm 
  developed by BD Genomics. 
* The algorithm was originally developed for clustering single-cell gene expression profiles, but it can be readily applied to clustering more general 
  high-dimensional data, as far as the input data conform to the required format (see section *Example usage* below). 
* Brief description of the clustering algorithm:  
  After preprocessing the input data and generating a distance matrix, the clustering algorithm first runs hierarchical clustering on the distance matrix to 
  generate a dendrogram, and then goes through the following 2 phases to identify statistically significant clusters. To aid visualization, the bh-tSNE algorithm is performed 
  to project the high-dimensional data to 2-dim space. See van der Maaten, LJP. Accelerating t-SNE using Tree-Based Algorithms. *Journal of Machine Learning 
  Research*. 2014;15(Oct):3221-3245.
    * *Phase 1: splitting and testing*  
      Starting from the top of the dendrogram, the tree is split into two subtrees. Intuitively, the split corresponds to a cluster
      being split into two candidate subclusters. The split is scored with the p-value obtained from statistics testing. All possible splits are performed and 
      their scores are recorded. Various thresholds of p-value cutoffs are attempted as the split criterion to generate multiple versions of the clustering results.
      Splitting results (sets of cluster labels) are kept and subjected to the next merging step.
    * *Phase 2: Merging*  
      Using the labels generated during splitting and testing, the merging phase decides if any of these clusters should be combined to form one cluster. 
      The splitting phase can produce small clusters of a few data points each. This phase cleans up the smaller clusters by merging them with 
      larger clusters.        
* For more details on the algorithm, refer to the **BD Genomics Bioinformatics Handbook** (Doc ID: 54169).
* Version 1.0 

### How do I get set up? ###

* Dependencies (python modules):
    * numpy
    * pandas
    * sklearn
    * scipy
    * matplotlib
    * tsne
    * networkx and community: not required by default, only needed for community detction. networkx is also used for maxed-weight-matching (as a 
      metric of how close two sets of labels are)
* To use the package, clone the repository into your local directory, 
```
git clone https://bitbucket.org/CRSwDev/dendrosplit.git
```

  and run:
```
python setup.py install
```
      
### Example usage ###

* [Synthetic data with ground truth of labels](examples/example_synthetic.md)
* [An example data in dense matrix format](examples/example_denseMatrix.md)
* [An example data in sparse matrix format](examples/example_sparseMatrix.md)

### More information ###

* Zhang JM, Fan J, Fan HC, Rosenfeld D, Tse DN, [*An Interpretable Framework for Clustering Single-Cell RNA-Seq Datasets*](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2092-7). BMC Bioinformatics. 2018 19:93. 

### License ###

* This work is licensed and distributed under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-nc-sa/4.0/).

### Who do I talk to? ###

* Contact David Rosenfeld at <david.rosenfeld@bd.com> if you have questions.
