import setuptools


if __name__ == '__main__':
    setuptools.setup(
        name='dendrosplit',
        version='1.0',
        # This automatically detects the packages in the specified
        # (or current directory if no directory is given).
        packages=setuptools.find_packages("src"),
        package_dir={'': 'src'},
        # The entry points are the big difference between
        # setuptools and distutils, the entry points make it
        # possible to extend setuptools and make it smarter and/or
        # add custom commands.
        entry_points={
            # The following would make these functions callable as
            # standalone scripts. In this case it would add the
            # spam command to run in your shell.
            'console_scripts': [
                'dendrosplit_pipeline = dendrosplit.dendrosplit_pipeline:main'
            ],
        },
        # Packages required to use this one, it is possible to
        # specify simply the application name, a specific version
        # or a version range. The syntax is the same as pip
        # accepts.
        install_requires=['cython', 'pandas', 'numpy', 'scipy', 'sklearn', 'matplotlib', 'tsne', 'networkx', 'community'],
        # Packages required to install this package, not just for
        # running it but for the actual install. These will not be
        # installed but only downloaded so they can be used during
        # the install. The pytest-runner is a useful example:
        setup_requires=[],
        # The requirements for the test command. Regular testing
        # is possible through: python setup.py test The Pytest
        # module installs a different command though: python
        # setup.py pytest
        tests_require=['pytest'],
        # All of the following fileds are PyPI metadata fields.
        # When registering a package at PyPI this is used as
        # information on the package page.
        author='David Rosenfeld',
        author_email='david.rosenfeld@bd.com',
        # This should be a short description (one line) for the
        # package
        description='Scripts for performing clustering analysis using the Recursive DendroSplit algorithm developed by BD Genomics',
        # For this parameter I would recommend including the
        # README.rst
        long_description="""This package can be used for performing the clustering analysis using the DendroSplit (Recursive Dendrogram Splitting)
        algorithm developed by BD Genomics. The algorithm was originally developed for clustering single-cell gene expression 
        profiles, but it can be readily applied to clustering more general high-dimensional data, as far as the input data conform to 
        the required format. For more details on the algorithm, refer to the BD Genomics Bioinformatics Handbook (Doc ID: 54169). """,
        license = 'CC BY-NC-SA',
        # Homepage url for the package
)
