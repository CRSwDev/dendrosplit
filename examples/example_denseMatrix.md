### Run dendrosplit clustering on an example data in dense matrix format

* This example dataset ([example_MolsPerCell.csv](data/example_MolsPerCell.csv)) has been generated from the BD Rhapsody pipeline and stored as
  a matrix of gene/transcript expression values where the rows correspond to cells and the columns correspond to
  features/genes. This particular example data contains expression values for 1176 cells x 397 genes.
* The top 8 rows are the header lines that have to be started with symbol '##'; and the 9th row gives the column header. 
* Starting from the 10th row are the actual expression data where the first column gives the cell identity
  information, and the second column onwards store the gene/transcript expression values.

* To perform the dendrosplit clustering analysis on this dataset, assuming you have already [cloned the repo and set up](https://bitbucket.org/CRSwDev/dendrosplit/overview), you run the following commands,
```sh
$ cd examples/data
$ python ../../src/dendrosplit/dendrosplit_pipeline.py --datatable example_MolsPerCell.csv --startcol-dm 1
```

* After the clustering analysis is done, a folder called *ClusteringAnalysis* and the corresponding zip archive [*ClusteringAnalysis.zip*](results/denseMatrix_ClusteringAnalysis.zip) will be generated.
  The folder stores all the output files/plots from the clustering analysis. For example,
    * *tSNE coordiates*  
    file [<sample_name>_bh-tSNEcoordinates.csv](results/denseMatrix_bh-tSNEcoordinates.csv) gives the coordinates after projecting the input
    data onto 2-dimension using the tSNE algorithm. The coordinates can be used to visualize cells. The order of
    cells/rows listed in this file is in the same order of cells/rows in the input data file.
    * *cluster labels*  
    file [<sample_name>_<num_clusters>_Labels.csv](results/denseMatrix_4-Clusters_Labels.csv) gives the assignment of an integer representing
    the cluster label to each cell. The order of cells/rows listed in this file is in the same order of cells/rows in the
    input data file. The value -1 means singletons, which are cells not assigned to any of the clusters. You may use
    this file and the coordinate file for additional clustering analysis.
    * *tSNE plot*  
    file [<sample_name>_<num_clusters>_tSNE.png](results/denseMatrix_4-Clusters_tSNE.png) is a visualization of the tSNE plot with cells colored
    according to cluster label.
    * Note that you may get different tSNE coordinates and tSNE plot in your run from the file/plot given here. It
    is because different runs with the tSNE algorithm may give you different results. However, the cluster labels
    determined by the dendrosplit algorithm will always give the same results as far as the input data are the same.
  
    For more information on the output files from the clustering analysis, refer to the **BD Genomics Bioinformatics Handbook** (Doc ID: 54169).
