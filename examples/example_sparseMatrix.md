### Run dendrosplit clustering on an example data in sparse matrix format

* This example dataset ([example_Expression_Data.st](data/example_Expression_Data.st)) has been generated from the BD Rhapsody pipeline and stored in
  a sparse matrix format that only export non-zero expression values for cell-gene combinations. 
* The top 8 rows are the header lines that have to be started with symbol '##'; and the 9th row gives the column header. 
* Starting from the 10th row are the actual data where the first column gives the cell identity information, 
  the second is the gene/transcript, third column onwards store the gene expression values. For 
  clustering analysis, only one column data is needed. For data generated from BD Rhapsody workflow, it is 
  recommended to use DBEC adjusted molecule counts data (7th column in the sparse expression file) if the 
  sequencing has been done deep enough. 

* To perform the dendrosplit clustering analysis on this dataset, assuming you have already [cloned the repo and set up](https://bitbucket.org/CRSwDev/dendrosplit/overview), you run the following commands,
```sh
$ cd examples/data
$ python ../../src/dendrosplit/dendrosplit_pipeline.py --datatable example_Expression_Data.st --col-sm 6 
```

* After it is done, a folder called *ClusteringAnalysis* and the corresponding zip archive [*ClusteringAnalysis.zip*](results/sparseMatrix_ClusteringAnalysis.zip) will be generated.
  The folder stores all the output files/plots from the clustering analysis. For example,
    * *tSNE coordiates*  
    file [<sample_name>_bh-tSNEcoordinates.csv](results/sparseMatrix_bh-tSNEcoordinates.csv) gives the coordinates after projecting the input
    data onto 2-dimension using the tSNE algorithm. The coordinates can be used to visualize cells. 
    * *cluster labels*  
    file [<sample_name>_<num_clusters>_Labels.csv](results/sparseMatrix_5-Clusters_Labels.csv) gives the assignment of an integer representing
    the cluster label to each cell. The value -1 means singletons, which are cells not assigned to any of the 
    clusters. You may use this file and the coordinate file for additional clustering analysis.
    * *tSNE plot*  
    file [<sample_name>_<num_clusters>_tSNE.png](results/sparseMatrix_5-Clusters_tSNE.png) is a visualization of the tSNE plot with cells colored
    according to cluster label.
    * Note that you may get different tSNE coordinates and tSNE plot in your run from the file/plot given here. It
    is because different runs with the tSNE algorithm may give you different results. However, the cluster labels
    determined by the dendrosplit algorithm will always give the same results as far as the input data are the same.
  
    For more information on the output files from the clustering analysis, refer to the **BD Genomics Bioinformatics Handbook** (Doc ID: 54169).
