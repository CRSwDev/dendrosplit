### Run dendrosplit clustering on a synthetic dataset

* This synthetic dataset ([synthetic_dim32.csv](data/synthetic_dim32.csv)) is 
  a 32-dimensional data that contain 1024 data points. The data has 16 Gaussian clusters with each cluster having equal number (64) of data points. The data is downloadable from [here](https://cs.joensuu.fi/sipu/datasets/)
* The top 2 rows are the header lines that have to be started with symbol '##'; and the 3rd row gives the column header. 
* Starting from the 4th row are the actual data where the first column gives the ground truth of cluster labels.

* To perform the dendrosplit clustering analysis on this dataset, assuming you have already [cloned the repo and set up](https://bitbucket.org/CRSwDev/dendrosplit/overview), you run the following commands,
```sh
$ cd examples/data
$ python ../../src/dendrosplit/dendrosplit_pipeline.py --datatable synthetic_dim32.csv --startcol-dm 1
```

* After the clustering analysis is done, a folder called *ClusteringAnalysis* and the corresponding zip archive [*ClusteringAnalysis.zip*](results/synthetic_ClusteringAnalysis.zip) will be generated.
  The folder stores all the output files/plots from the clustering analysis. For example,
    * *tSNE coordiates*  
    file [<sample_name>_bh-tSNEcoordinates.csv](results/synthetic_bh-tSNEcoordinates.csv) gives the coordinates after projecting the input
    data onto 2-dimension using the tSNE algorithm. The coordinates can be used to visualize data points. The order of rows listed in this file is in the same order of rows in the input data file.
    * *cluster labels*  
    file [<sample_name>_<num_clusters>_Labels.csv](results/synthetic_16-Clusters_Labels.csv) gives the assignment of an integer representing
    the cluster label to each datapoint/row. The order of rows listed in this file is in the same order of rows in the
    input data file. You may use this file and the coordinate file for additional clustering analysis.
    * *tSNE plot*  
    file [<sample_name>_<num_clusters>_tSNE.png](results/synthetic_16-Clusters_tSNE.png) is a visualization of the tSNE plot with cells colored
    according to cluster label.
    * Note that you may get different tSNE coordinates and tSNE plot in your run from the file/plot given here. It
    is because different runs with the tSNE algorithm may give you different results. However, the cluster labels
    determined by the dendrosplit algorithm will always give the same results as far as the input data are the same.
  
    For more information on the output files from the clustering analysis, refer to the **BD Genomics Bioinformatics Handbook** (Doc ID: 54169).
